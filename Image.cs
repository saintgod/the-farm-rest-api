﻿using Newtonsoft.Json;

namespace Thefarm
{
    public class Image
    {
        //Name of image
        public string fileName { get; set; }
        //will be converted to Base64 byte array stored as JSON string
        public byte[] imageData { get; set; }


    }
}
