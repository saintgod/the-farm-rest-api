﻿using Grpc.Core;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Thefarm.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ImageController : Controller
    {     
        // POST: ImageController/Create

        /// <summary>
        /// Projektet:
        /// Spara namnet på bilden och 64bit encoding
        /// Spara ny bild: 
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        [HttpPost("AddNew")]
        [ValidateAntiForgeryToken]
        public bool Create([FromBody] Image image)
        {
            string imageFile = System.IO.File.ReadAllText(@".\imageInfo.json");
            List<Image> images = JsonConvert.DeserializeObject<List<Image>>(imageFile);
            foreach(Image img in images) {
                if(img.fileName == image.fileName)
                {
                    return false;
                }
            }
            images.Add(image);
            System.IO.File.WriteAllText(@".\imageInfo.json", JsonConvert.SerializeObject(images));
            return true;

        }
        [HttpGet("All")]
        [ValidateAntiForgeryToken]
        public List<Image> GetImages()
        {
            string imageFile = System.IO.File.ReadAllText(@".\imageInfo.json");
            List<Image> images = JsonConvert.DeserializeObject<List<Image>>(imageFile);
            return images;
        }
    }
}
